﻿using System;
using System.Collections.ObjectModel;
using HW4_ListView_Dodia001.Models;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW4_ListView_Dodia001
{
    public partial class AthleteListView : ContentPage
    {
        public AthleteListView()
        {
            InitializeComponent();
            AthletesList();
        }
        void HandleDeleteClicked(Object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var athlete = (AthleteItem)menuItem.CommandParameter;
            

        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            AthletesListView.IsRefreshing = false;
        }

        void HandleInfoClicked(Object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var athlete = (AthleteItem)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreAthleteInfo(athlete));

        }
        void Handle_ItemTapped(object sender, System.EventArgs e )
        {
            var listView = (ListView)sender;
            AthleteItem itemTapped = (AthleteItem)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }

        private void AthletesList()
        {
            var listOfAthletes = new ObservableCollection<AthleteItem>();

            var athlete1 = new AthleteItem
            {
                Name = " Lionel Messi",
                Game = " Soccer Player (Argentina)",
                Image = "Lionel.png",
                url = "https://en.wikipedia.org/wiki/Lionel_Messi",
                Born = "Born:June 24,1987",
                Height = "Height: 5'7",
                Team = "Team: FC Barcelona, Argentina"
            };
            listOfAthletes.Add(athlete1);

            var athlete2 = new AthleteItem()
            {
                Name = " Tiger Woods",
                Game = " Golfer(America)",
                Image = "Tiger.png",
                url = "https://en.wikipedia.org/wiki/Tiger_Woods",
                Born = "Born: December 13,1975",
                Height = "Height: 5'6",
                Team = "Team: American Profession Golfer"
            }; 
            listOfAthletes.Add(athlete2);

            var athlete3 = new AthleteItem()
            {
                Name = " LeBron James",
                Game = " Basketball Player(America)",
                Image = "Lebro.png",
                url = "https://en.wikipedia.org/wiki/LeBron_James",
                Born = "Born: December 30,1984",
                Height = "Height: 6'8",
                Team = "Team: Los Angeles Lakers"
            };
            listOfAthletes.Add(athlete3);

            var athlete4 = new AthleteItem()
            {
                Name = " Roger Federer",
                Game = "  Tennis Player(Switzerland)",
                Image = "Roger.png",
                url = "https://en.wikipedia.org/wiki/Roger_Federer",
                Born = "Born: August 8,1981",
                Height = "Height: 6'1",
                Team = "Team: Switzerland professional player"
            };
            listOfAthletes.Add(athlete4);

            var athlete5 = new AthleteItem()
            {
                Name = " Cristiano Ronaldo",
                Game = " Soccer Player (Portugusse)",
                Image = "Crist.png",
                url = "https://en.wikipedia.org/wiki/Cristiano_Ronaldo",
                Born = "Born: February 5,1985",
                Height = "Height: 6'2",
                Team = "Team: Juventus F.C. ,Portugal"
            };
            listOfAthletes.Add(athlete5);
       
            AthletesListView.ItemsSource = listOfAthletes;
            
        }
        
    }
}
