﻿using System;
using System.Collections.Generic;
using HW4_ListView_Dodia001.Models;
using Xamarin.Forms;

namespace HW4_ListView_Dodia001
{
    public partial class MoreAthleteInfo : ContentPage
    {
        public MoreAthleteInfo()
        {
            InitializeComponent();
        }

        public MoreAthleteInfo(AthleteItem athlete)
        {
            InitializeComponent();

            BindingContext = athlete;
        }
    }
}
