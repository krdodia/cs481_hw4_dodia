﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW4_ListView_Dodia001
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        void OnClickNavigate(Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new AthleteListView());
        }
    }
}
